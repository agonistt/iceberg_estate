<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 *  Authentication Routes
 */
Route::group(['middleware' => 'auth:api'], function ($router) {
    Route::post('register', \App\Http\Controllers\RegisterController::class.'@register');
    Route::post('login',    \App\Http\Controllers\AuthController::class.'@login');
    Route::post('logout',   \App\Http\Controllers\AuthController::class.'@logout');
    Route::post('refresh',  \App\Http\Controllers\AuthController::class.'@refresh');
    Route::post('me',       \App\Http\Controllers\AuthController::class.'@me');


    Route::group([ 'prefix' => 'contact' ], function () {
        Route::get('/',            \App\Http\Controllers\ContactController::class."@index");
        Route::post('store',        \App\Http\Controllers\ContactController::class."@store");
        Route::post('/{id}',         \App\Http\Controllers\ContactController::class.'@update');
        Route::post('delete/{id}',   \App\Http\Controllers\ContactController::class.'@destroy');
    });
    Route::group([ 'prefix' => 'appointment' ], function () {
        Route::get('/',            \App\Http\Controllers\AppointmentController::class."@index");
        Route::post('store',        \App\Http\Controllers\AppointmentController::class."@store");
        Route::post('/{id}',         \App\Http\Controllers\AppointmentController::class.'@update');
        Route::post('delete/{id}',   \App\Http\Controllers\AppointmentController::class.'@destroy');
    });
});


