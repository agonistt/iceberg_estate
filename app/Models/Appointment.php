<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'contact_id', 'post_code', 'start_at', 'should_leave_at', 'should_return_at',
    ];

    protected $dates = [
        'start_at',
        'should_leave_at',
        'should_return_at',
        'created_at',
        'updated_at',
    ];
}
