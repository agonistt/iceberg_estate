<?php

namespace App\Http\Controllers;

use App\Repositories\Eloquent\AppointmentRepository;
use App\Repositories\Eloquent\ContactRepository;
use App\Repositories\Eloquent\UserRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{

    private $contactRepository;

    public function __construct(
        ContactRepository     $contactRepository
    )
    {
        $this->contactRepository = $contactRepository;

    }

    public function index(Request $request)
    {
        $result = $this->contactRepository->paginate($request->input('page') ?? 1);
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:contacts'],
            'phone' => 'required|digits:11|numeric|unique:contacts',
        ]);

        if ($validation->fails()) {
            return response()->json(['status' => false, 'errors' => $validation->errors()], 400);
        }
        try {
            $result = $this->contactRepository->create($request->all());
        } catch (\Exception $err) {
            Log::error($err->getMessage(), [$err->getTraceAsString()]);
            return response()->json(['status' => false, $err->getMessage()], 500);
        }

        return response()->json([
            'status' => true,
            'data'   => $result
            ]);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => ['required', 'numeric', 'digits:11'],
        ]);

        if ($validation->fails()){
            return response()->json(['status' => false, 'errors' => $validation->errors()], 400);
        }

        try {
            $result = $this->contactRepository->update($id, $request->all());
        } catch (\Exception $err) {
            return response()->json(['status' => false, 'errors'=> $err->getMessage()], 500);
        }

        return response()->json([
            'status' => true,
            'data'   => $result
        ]);

    }

    public function destroy($id)
    {
        try {
            $this->contactRepository->delete($id);
        } catch (\Exception $err) {
            return response()->json(['status' => false, 'errors'=> $err->getMessage()], 500);
        }

        return response()->json([
            'status' => true,
            'id'   => $id
        ]);

    }


}
