<?php

namespace App\Http\Controllers;

use App\Repositories\Eloquent\AppointmentRepository;
use App\Repositories\Eloquent\ContactRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Services\GoogleMap\Client\GoogleMapClient;
use App\Services\GoogleMap\LocationService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{

    private $contactRepository;
    private $appointmentRepository;

    public function __construct(
        ContactRepository     $contactRepository,
        AppointmentRepository $appointmentRepository
    )
    {
        $this->contactRepository = $contactRepository;
        $this->appointmentRepository = $appointmentRepository;

    }

    public function index(Request $request)
    {
        $result = $this->appointmentRepository->paginate($request->input('page') ?? 1);
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'user_id' => ['required', 'numeric', 'exists:users,id'],
            'contact_id' => ['required', 'numeric', 'exists:contacts,id'],
            'start_at' => ['required', 'date', 'date_format:Y-m-d H:i:s','after:now'],
        ]);

        if ($validation->fails()) {
            return response()->json(['status' => false, 'errors' => $validation->errors()], 400);
        }
        try {
//            dd('asd',$this->contactRepository);
            $result = $this->appointmentRepository->create($request->all());
        } catch (\Exception $err) {
            Log::error($err->getMessage(), [$err->getTraceAsString()]);
            return response()->json(['status' => false, $err->getMessage()], 500);
        }

        return response()->json([
            'status' => true,
            'data'   => $result
        ]);
    }

    public function update(Request $request, $id)
    {

        $validation = Validator::make($request->all(), [
            'user_id' => ['required', 'numeric', 'exists:users,id'],
            'contact_id' => ['required', 'numeric', 'exists:contacts,id'],
        ]);

        if ($validation->fails()){
            return response()->json(['status' => false, 'errors' => $validation->errors()], 400);
        }

        try {
            $result = $this->appointmentRepository->update($id, $request->all());
        } catch (\Exception $err) {
            return response()->json(['status' => false, 'errors'=>$err->getMessage()], 500);
        }

        return response()->json([
            'status' => true,
            'data'   => $result
        ]);

    }

    public function destroy($id)
    {
        try {
            $this->contactRepository->delete($id);
        } catch (\Exception $err) {
            return response()->json(['status' => false, 'errors'=> $err->getMessage()], 500);
        }

        return response()->json([
            'status' => true,
            'id'   => $id
        ]);

    }


}
