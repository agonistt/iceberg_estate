<?php

namespace App\Providers;

use App\Services\GoogleMap\Client\GoogleMapClient;
use App\Services\IysHermes\Client\IysHermesClient;
use Illuminate\Support\ServiceProvider;

class GoogleMapsApiServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            GoogleMapClient::class,
            function ($client) {
                return new GoogleMapClient(
                    [
                        'base_uri'    => "https://maps.googleapis.com/",
                    ]
                );
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }


}
