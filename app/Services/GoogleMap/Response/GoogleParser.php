<?php


namespace App\Services\GoogleMap\Response;

use App\Services\GoogleMap\Response\ResponseArray;
use App\Services\GoogleMap\Response\ResponseObject;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class GoogleParser
{
    public $req          = [];
    public $parserSchema = [];

    /**
     * GoogleParser constructor.
     * @param $req
     * @param $parserSchema
     */
    public function __construct($req, $parserSchema)
    {
        $this->req          = $req;
        $this->parserSchema = $parserSchema;
    }

    public function parse()
    {
        $data   = json_decode($this->req->getBody()->getContents());
        $status = $this->req->getStatusCode();

        if ($this->parserSchema instanceof \Closure) {
        } else {
            if (is_string($this->parserSchema)) {
                $node = $this->extractNode($data);
                $meta = $this->extractMeta($data);
                return $this->createResponse($node, $status, $meta);
            } else {
                if (is_null($this->parserSchema)) {
                    return $this->createResponse($data, $status);
                }
            }
        }
    }

    private function createResponse($data, $status, $meta = null)
    {
        if (is_array($data)) {
            return new ResponseArray($data, $status, $meta);
        } else {
            if (!is_null($data)) {
                return new ResponseObject($data, $status, $meta);
            } else {
                return new ResponseObject($data, $status, $meta);
            }
        }
    }

    private function extractNode($data)
    {
        $paths       = explode('.', $this->parserSchema);
        $currentNode = null;
        foreach ($paths as $path) {
            if (!$currentNode) {
                $currentNode = $data->{$path};
            } else {
                $currentNode = $currentNode->{$path};
            }
        }
        return $currentNode;
    }

    private function extractMeta($data)
    {
        $objectKeys = get_object_vars($data);
        $path       = explode(".", $this->parserSchema);
        $pathCount  = count($path);
        $meta       = [];

        foreach ((array)$data as $key => $property) {
            if ($key !== $path[0]) {
                $meta["$key"] = $property;
            }
        }

        /**
         * iç içe node verildiyse diye böyle bir tampon işlem var.. anlık ihtiyacı karşılıyor
         */
        if ($pathCount >1) {
            foreach ((array)$data->{$path[0]} as $key => $property) {
                if ($key !== $path[1]) {
                    $meta["$key"] = $property;
                }
            }
        }

        return (object)$meta;
    }

}
