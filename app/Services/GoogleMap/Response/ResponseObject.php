<?php


namespace App\Services\GoogleMap\Response;


use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;

class ResponseObject
{
    public $status;
    public $item;
    public $meta;

    public function __construct($item, $status = null, $meta = null)
    {
        $this->item   = (object)$item;
        $this->meta   = (object)$meta;
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    private function hasAttribute($key)
    {
        return isset($this->item->{$key});
    }

    private function getAttribute($key)
    {
        return $this->item->{$key} ?? null;
    }

    public function getMeta($key=null)
    {
        if (!$key) {
            return $this->meta;
        }
        return $this->meta->{$key};
    }

    public function __get($key)
    {
        if ($this->hasAttribute($key)) {
            return $this->getAttribute($key);
        } else {
            return null;
        }
    }

    public function toJson()
    {
        return json_encode($this->item);
    }

    public function __toString()
    {
        return (string)$this->toJson();
    }

}
