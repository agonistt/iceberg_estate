<?php


namespace App\Services\GoogleMap\Response;

use Illuminate\Support\Collection;

class ResponseArray extends Collection
{
    public $status;
    public $items;
    public $meta;

    public function __construct($items = [], $status = null, $meta = null)
    {
        $this->items  = $items;
        $this->status = $status;
        $this->meta   = $meta;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getMeta($key = null)
    {
        if (!$key) {
            return $this->meta;
        }
        return $this->meta->{$key};
    }
}
