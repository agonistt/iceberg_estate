<?php

namespace App\Services\GoogleMap;

use App\Services\GoogleMap\Client\GoogleMapClient;
use App\Services\GoogleMap\Response\GoogleParser;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\TransferStats;
use Illuminate\Support\Facades\Log;

class Service
{
    protected $client;

    public function __construct()
    {
        $this->client = app(GoogleMapClient::class);
    }

    public function get(string $uri, array $options = [], $parserSchema = null)
    {
        try {
            $req = $this->client->get(
                $uri,
                array_merge(
                    $options,
                    $this->debugGet()
                )
            );
            return (new GoogleParser($req, $parserSchema))->parse();
        } catch (\Exception $exception) {
            throw  $exception;
        }
    }

    public function post(string $uri, array $options = [], $parserSchema = null)
    {
        try {
            $req = $this->client->post(
                $uri,
                array_merge(
                    $options,
                    $this->debugPost($options)
                )
            );
            return (new GoogleParser($req, $parserSchema))->parse();
        } catch (\Exception $exception) {
            throw  $exception;
        }
    }

    private function debugPost($options)
    {
        if (env('APP_DEBUG') === true) {
            return [
                'on_stats' => function (TransferStats $stats) use ($options) {
                    $postBody   = isset($options) && isset($options['form_params']) ? $options['form_params'] : [];
                    $statusCode = "";
                    if ($stats->hasResponse()) {
                        $statusCode = $stats->getResponse()->getStatusCode();
                    }
                    Log::debug(
                        "Requesting POST {$stats->getEffectiveUri()} " . http_build_query(
                            $postBody
                        ) . " $statusCode",
                        $postBody
                    );
                }
            ];
        }
        return [];
    }

    private function debugGet()
    {
        if (env('APP_DEBUG') === true) {
            return [
                'on_stats' => function (TransferStats $stats) {
                    $statusCode = "";
                    if ($stats->hasResponse()) {
                        $statusCode = $stats->getResponse()->getStatusCode();
                    }
                    Log::debug(
                        "Requesting GET {$stats->getEffectiveUri()} " . " $statusCode"
                    );
                }
            ];
        }
        return [];
    }

    public function options($queries = [], $config = [])
    {
        $blueprint = [
            'form_params' => $queries,//post
        ];
        return $blueprint;
    }

    public function forGetOptions($queries = [], $config = [])
    {
        $blueprint = [
            'query' => $queries //get
        ];
        return $blueprint;
    }


}
