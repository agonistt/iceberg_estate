<?php


namespace App\Services\GoogleMap\Client;


use GuzzleHttp\Client;

class GoogleMapClient extends Client
{

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }
}
