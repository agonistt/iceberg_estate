<?php

namespace App\Services\GoogleMap;

class LocationService extends Service
{

    /**
     * @return Response\ResponseArray|Response\ResponseObject
     */
    public function searchPostCode()
    {
        return $this->get('maps/api/geocode/json');
    }


    /**
     * @return Response\ResponseArray|Response\ResponseObject
     */
    public function direction($destination)
    {
        $origin = env('OFFICE_POST_CODE');
        $apiKey = env('GOOGLE_MAPS_API_KEY');

        return $this->get("maps/api/directions/json?origin=$origin&destination=$destination&key=$apiKey");
    }


}
