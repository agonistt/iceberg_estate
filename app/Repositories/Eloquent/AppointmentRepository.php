<?php

namespace App\Repositories\Eloquent;

use App\Models\Appointment;
use App\Models\Contact;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use App\Services\GoogleMap\LocationService;
use Carbon\Carbon;
use http\Exception\InvalidArgumentException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AppointmentRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param Appointment $model
     */
    public function __construct(Appointment $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function paginate($page=1,$limit=15): LengthAwarePaginator
    {
        return $this->model->take($limit)
            ->skip(($page - 1) * $limit)
            ->paginate();
    }
    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes): Model
    {
        $locationService = new LocationService();
        $res = $locationService->direction('E18');
        $now = Carbon::now();
        $after=  Carbon::parse(strtotime($res->item->routes[0]->legs[0]->duration->text));
        $roadTime = $now->diffInMinutes($after);

        $attributes['should_leave_at'] = Carbon::parse($attributes['start_at'])->subMinutes($roadTime);
        $attributes['should_return_at'] = Carbon::parse($attributes['start_at'])->addHour()->addMinutes($roadTime);


        return $this->model->create($attributes);
    }

    /**
     * @param $id
     * @param array $data
     * @return Model
     */
    public function update($id, array $data): Model
    {
        if (!$this->exists($id))
            throw new ModelNotFoundException('Model Not Found');

        $locationService = new LocationService();
        $res = $locationService->direction('E18');
        $now = Carbon::now();
        $after=  Carbon::parse(strtotime($res->item->routes[0]->legs[0]->duration->text));
        $roadTime = $now->diffInMinutes($after);

        DB::beginTransaction();


        try {
            $model = $this->find($id);
            $data['should_leave_at'] = Carbon::parse($model->start_at)->subMinutes($roadTime);
            $data['should_return_at'] = Carbon::parse($model->start_at)->addHour()->addMinutes($roadTime);

            $model->fill($data);
            $model->save();

        } catch (\Exception $err) {
            DB::rollBack();
            Log::error($err->getMessage(), [$err->getTraceAsString()]);
            throw new InvalidArgumentException('Unable to update');
        }

        DB::commit();

        return $model;
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id) : Bool
    {

        DB::beginTransaction();
        $model = $this->find($id);
        if (!$model)
            throw new ModelNotFoundException('Model Not Found');

        try {
            $model->delete();
        } catch (\Exception $err) {
            DB::rollBack();
            Log::error($err->getMessage(), [$err->getTraceAsString()]);
            throw new InvalidArgumentException('Unable to update');
        }

        DB::commit();

        return true;
    }
}
