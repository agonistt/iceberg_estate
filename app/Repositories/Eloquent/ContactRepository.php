<?php

namespace App\Repositories\Eloquent;

use App\Models\Contact;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use http\Exception\InvalidArgumentException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ContactRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(Contact $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function paginate($page=1,$limit=15): LengthAwarePaginator
    {
        return $this->model->take($limit)
            ->skip(($page - 1) * $limit)
            ->paginate();
    }

    public function update($id, array $data): Model
    {
        if (!$this->exists($id))
            throw new ModelNotFoundException('Model Not Found');

        DB::beginTransaction();

        try {
            $contact = $this->find($id);
            $contact->fill($data);
            $contact->save();

        } catch (\Exception $err) {
            DB::rollBack();
            Log::error($err->getMessage(), [$err->getTraceAsString()]);
            throw new InvalidArgumentException('Unable to update');
        }

        DB::commit();

        return $contact;
    }

    public function delete($id) : Bool
    {

        DB::beginTransaction();
        $contact = $this->find($id);
        if (!$contact)
            throw new ModelNotFoundException('Model Not Found');

        try {
            $contact->delete();
        } catch (\Exception $err) {
            DB::rollBack();
            Log::error($err->getMessage(), [$err->getTraceAsString()]);
            throw new InvalidArgumentException('Unable to update');
        }

        DB::commit();

        return true;
    }
}
