<?php
namespace App\Repositories;

use Illuminate\Support\Collection;

interface AppointmentRepositoryInterface
{
    public function paginate(): Collection;
}
