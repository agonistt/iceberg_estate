<?php
namespace App\Repositories;

use Illuminate\Support\Collection;

interface ContactRepositoryInterface
{
    public function paginate(): Collection;
}
